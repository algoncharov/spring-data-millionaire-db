package springdatamillionaire.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDTO {

    public UserDTO(long id,String name, String surname, String userRoleName,long userRoleId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.userRoleName = userRoleName;
        this.userRoleId = userRoleId;
    }

    public UserDTO() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserRoleName() {
        return userRoleName;
    }


    private long id;
    private String name;
    private String surname;
    private String userRoleName;


    private long userRoleId;

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public long getUserRoleId() {

        return userRoleId;
    }

    public void setUserRoleId(long userRoleId) {
        this.userRoleId = userRoleId;
    }
}
