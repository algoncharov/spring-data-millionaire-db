package springdatamillionaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.entity.User;
import springdatamillionaire.entity.UserRole;
import springdatamillionaire.repo.UserRepository;
import springdatamillionaire.repo.UserRoleRepository;

import java.util.ArrayList;
import java.util.List;



@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserDTO create(UserDTO userDTO) {
        return toDTO(userRepository.save(fromDTO(userDTO)));

    }

    public List<UserDTO> findall()
    {
        List<User> users = userRepository.findAll();
        ArrayList<UserDTO> userDTOS = new ArrayList<UserDTO>();
        for (User user : users) {
            userDTOS.add(toDTO(user));
        }
        return userDTOS;
    }


    public UserDTO findById(long id)
    {
        return toDTO(userRepository.findOne(id));
    }

    public UserDTO update(UserDTO userDTO,long userId)
    {
        User updated = userRepository.findOne(userId);
        updated.setName(userDTO.getName());
        updated.setSurname(userDTO.getSurname());
        updated.setUserRole(userRoleRepository.findOne(userDTO.getUserRoleId()));
        return toDTO(userRepository.save(updated));
    }

    public UserDTO delete(long id) {
        UserDTO deleted = toDTO(userRepository.findOne(id));
        userRepository.delete(id);
        return deleted;
    }

    private User fromDTO(UserDTO userDTO)
    {
        UserRole userRole = userRoleRepository.findOne(userDTO.getUserRoleId());

        return new User(userDTO.getName(),userDTO.getSurname(), userRole );
    }

    private UserDTO toDTO(User user)
    {
        return new UserDTO(user.getId(),user.getName(),user.getSurname(),user.getUserRole().getName(),user.getUserRole().getId() );
    }

    public void delete_all()
    {
        userRoleRepository.deleteAll();
    }

}
