package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Answer {
    public Answer(String text, boolean is_right, Question question) {
        this.text = text;
        this.is_right = is_right;
        this.question = question;
    }

    public Answer() {
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isIs_right() {
        return is_right;
    }

    public void setIs_right(boolean is_right) {
        this.is_right = is_right;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String text;

    private boolean is_right;

    @OneToOne
    private Question question;
}
