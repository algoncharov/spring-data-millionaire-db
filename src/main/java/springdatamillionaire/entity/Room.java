package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class Room {
    public Room(User host, boolean active, String pass_hash, Set<User> users, Set<Question> questions) {
        this.host = host;
        this.active = active;
        this.pass_hash = pass_hash;
        this.users = users;
        this.questions = questions;
    }

    public Room() {
    }

    public long getId() {
        return id;
    }

    public User getHost() {
        return host;
    }

    public void setHost(User host) {
        this.host = host;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPass_hash() {
        return pass_hash;
    }

    public void setPass_hash(String pass_hash) {
        this.pass_hash = pass_hash;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    private User host;

    private boolean active;

    @NotNull
    private String pass_hash;

    @ManyToMany
    private Set<User> users;

    @ManyToMany(mappedBy = "rooms")
    private Set<Question> questions;
}
