package springdatamillionaire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")

public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    UserDTO create(@RequestBody @Valid UserDTO userDTO) {
        return userService.create(userDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    List<UserDTO> findAll() {
        return userService.findall();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    UserDTO findById(@PathVariable long userId) {
        return userService.findById(userId);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    UserDTO update(@RequestBody @Valid UserDTO userDTO,@PathVariable long userId) {
        return userService.update(userDTO,userId);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    UserDTO delete(@PathVariable long id) {
        return userService.delete(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    void deletAll() {
        userService.delete_all();
    }
}
