package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.QuestionType;

public interface QuestionTypeRepository extends CrudRepository<QuestionType, Long> {
}
