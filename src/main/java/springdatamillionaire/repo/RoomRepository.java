package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Room;
import springdatamillionaire.entity.User;

public interface RoomRepository extends CrudRepository<Room, Long> {
}
