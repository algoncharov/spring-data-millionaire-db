package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
