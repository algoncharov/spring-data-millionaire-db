package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Question;

public interface QuestionRepository extends CrudRepository<Question, Long> {
}
