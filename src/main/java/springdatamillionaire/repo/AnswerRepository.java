package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Answer;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
}
