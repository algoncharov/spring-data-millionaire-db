package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.AnswerOrder;

public interface AnswerOrderRepository extends CrudRepository<AnswerOrder, Long> {
}